#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# pybittrex - A Python wrapper for the Bittrex API.
# Copyright (C) 2017  Benjamin Hallouin
#
# This file is part of pybittrex.
#
# pybittrex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pybittrex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pybittrex.  If not, see <http://www.gnu.org/licenses/>.

import hashlib
import hmac
import http.client
import json
import time
import urllib.parse


class API(object):
    def __init__(self, api_key=None, api_secret=None):
        self.api_host = 'bittrex.com'
        self.api_version = 'v1.1'
        self.api_key = api_key
        self.api_secret = api_secret

        self.connection = None

    def _request(self, url, headers=None):
        """ Requests handler.

        .. note::
        This function shouldn't be used directly. Preferably use
        :py:meth:'request_public', :py:meth:'request_market' or
        :py:meth:'request_account' instead.

        :param url: requests URL
        :type url: str
        :param headers: requests headers
        :type headers: dict
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        if headers is None:
            headers = {}

        if self.connection is None:
            self.connection = http.client.HTTPSConnection(self.api_host,
                                                          timeout=30)

        self.connection.request('GET', url, headers=headers)
        response = self.connection.getresponse()

        return json.loads(response.read().decode())

    def request_public(self, method, parameters=None):
        """ Requests on Bittrex 'public' API endpoint.

        :param method: 'public' Bittrex API method
        :type method: str
        :param parameters: dictionary of parameters for the method
        :type parameters: dict
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        if parameters is None:
            parameters = {}

        path = '/api/' + self.api_version + '/public/' + method
        query = urllib.parse.urlencode(parameters)
        url = 'https://' + self.api_host + path + '?' + query

        return self._request(url)

    def request_market(self, method, parameters=None):
        """ Requests on Bittrex 'market' API endpoint.

        :param method: 'market' Bittrex API method
        :type method: str
        :param parameters: dictionary of parameters for the method
        :type parameters: dict
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        if parameters is None:
            parameters = {}

        parameters['apikey'] = self.api_key
        parameters['nonce'] = int(time.time() * 1000)

        path = '/api/' + self.api_version + '/market/' + method
        query = urllib.parse.urlencode(parameters)
        url = 'https://' + self.api_host + path + '?' + query

        headers = {'apisign': hmac.new(self.api_secret.encode(), url.encode(),
                                       hashlib.sha512).hexdigest()}

        return self._request(url, headers=headers)

    def request_account(self, method, parameters=None):
        """ Requests on Bittrex 'account' API endpoint.

        :param method: 'account' Bittrex API method
        :type method: str
        :param parameters: dictionary of parameters for the method
        :type parameters: dict
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        if parameters is None:
            parameters = {}

        parameters['apikey'] = self.api_key
        parameters['nonce'] = int(time.time() * 1000)

        path = '/api/' + self.api_version + '/account/' + method
        query = urllib.parse.urlencode(parameters)
        url = 'https://' + self.api_host + path + '?' + query

        headers = {'apisign': hmac.new(self.api_secret.encode(), url.encode(),
                                       hashlib.sha512).hexdigest()}

        return self._request(url, headers=headers)

    def get_markets(self):
        """ Used to get the open and available trading markets at Bittrex along
        with other meta data.

        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        return self.request_public('getmarkets')

    def get_currencies(self):
        """ Used to get all supported currencies at Bittrex along with other
        meta data.

        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        return self.request_public('getcurrencies')

    def get_ticker(self, market):
        """ Used to get the current tick values for a market.

        :param market: a string literal for the market (ex: BTC-LTC)
        :type market: str
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = {'market': market}
        return self.request_public('getticker', parameters=parameters)

    def get_market_summaries(self):
        """ Used to get the last 24 hour summary of all active exchanges.

        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        return self.request_public('getmarketsummaries')

    def get_market_summary(self, market):
        """ Used to get the last 24 hour summary of all active exchanges.

        :param market: a string literal for the market (ex: BTC-LTC)
        :type market: str
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = {'market': market}
        return self.request_public('getmarketsummary', parameters=parameters)

    def get_order_book(self, market, type):
        """ Used to get the order book for a given market.

        :param market: a string literal for the market (ex: BTC-LTC)
        :type market: str
        :param type: buy, sell or both to identify the type of order book
        :type type: str
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = {'market': market, 'type': type}
        return self.request_public('getorderbook', parameters=parameters)

    def get_market_history(self, market):
        """ Used to retrieve the latest trades that have occurred for a specific
        market.

        :param market: a string literal for the market (ex: BTC-LTC)
        :type market: str
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = {'market': market}
        return self.request_public('getmarkethistory', parameters=parameters)

    def buy_limit(self, market, quantity, rate):
        """ Used to place a buy order in a specific market. Use buy_limit to
        place limit orders. Make sure you have the proper permissions set on
        your API keys for this call to work.

        :param market: a string literal for the market (ex: BTC-LTC)
        :type market: str
        :param quantity: the amount to purchase
        :type quantity: str
        :param rate: the rate at which to place the order
        :type rate: str
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = {'market': market, 'quantity': quantity, 'rate': rate}
        return self.request_market('buylimit', parameters=parameters)

    def sell_limit(self, market, quantity, rate):
        """ Used to place an sell order in a specific market. Use sell_limit to
        place limit orders. Make sure you have the proper permissions set on
        your API keys for this call to work.

        :param market: a string literal for the market (ex: BTC-LTC)
        :type market: str
        :param quantity: the amount to purchase
        :type quantity: str
        :param rate: the rate at which to place the order
        :type rate: str
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = {'market': market, 'quantity': quantity, 'rate': rate}
        return self.request_market('selllimit', parameters=parameters)

    def cancel(self, uuid):
        """ Used to cancel a buy or sell order.

        :param uuid: uuid of buy or sell order
        :type uuid: str
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = {'uuid': uuid}
        return self.request_market('cancel', parameters=parameters)

    def get_open_orders(self, market=None):
        """ Get all orders that you currently have opened. A specific market can
        be requested.

        :param market: a string literal for the market (ie. BTC-LTC)
        :type market: str, optional
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = None
        if market is not None:
            parameters = {'market': market}
        return self.request_market('getopenorders', parameters=parameters)

    def get_balances(self):
        """ Used to retrieve all balances from your account.

        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        return self.request_account('getbalances')

    def get_balance(self, currency):
        """ Used to retrieve the balance from your account for a specific
        currency.

        :param currency: a string literal for the currency (ex: LTC)
        :type currency: str
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = {'currency': currency}
        return self.request_account('getbalance', parameters=parameters)

    def get_deposit_address(self, currency):
        """ Used to retrieve or generate an address for a specific currency. If
        one does not exist, the call will fail and return ADDRESS_GENERATING
        until one is available.

        :param currency: a string literal for the currency (ex: BTC)
        :type currency: str
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = {'currency': currency}
        return self.request_account('getdepositaddress', parameters=parameters)

    def withdraw(self, currency, quantity, address, paymentid=None):
        """ Used to withdraw funds from your account.

        :param currency: a string literal for the currency (ex: BTC)
        :type currency: str
        :param quantity: the quantity of coins to withdraw
        :type quantity: str
        :param address: the address where to send the funds.
        :type address: str
        :param paymentid: used for CryptoNotes/BitShareX/Nxt (memo/paymentid)
        :type paymentid: str, optional
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = {'currency': currency, 'quantity': quantity,
                      'address': address}
        if paymentid is not None:
            parameters.update({'paymentid': paymentid})
        return self.request_account('withdraw', parameters=parameters)

    def get_order(self, uuid):
        """ Used to retrieve a single order by uuid.

        :param uuid: the uuid of the buy or sell order
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = {'uuid': uuid}
        self.request_account('getorder', parameters=parameters)

    def get_order_history(self, market):
        """ Used to retrieve your order history.

        :param market: a string literal for the market (ex: BTC-LTC).
        :type market: str, optional
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = None
        if market is not None:
            parameters = {'market': market}
        self.request_account('getorderhistory', parameters=parameters)

    def get_withdrawal_history(self, currency):
        """ Used to retrieve your withdrawal history.

        :param currency: a string literal for the currency (ie. BTC).
        :type currency: str, optional
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = None
        if currency is not None:
            parameters = {'currency': currency}
        self.request_account('getwithdrawalhistory', parameters=parameters)

    def get_deposit_history(self, currency):
        """ Used to retrieve your deposit history.

        :param currency: a string literal for the currency (ie. BTC).
        :type currency: str, optional
        :return: dictionary of deserialized Bittrex API json response
        :rtype: dict
        """
        parameters = None
        if currency is not None:
            parameters = {'currency': currency}
        self.request_account('getdeposithistory', parameters=parameters)
